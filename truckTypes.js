module.exports = {
  SPRINTER: 'SPRINTER',
  SMALL_STRAIGHT: 'SMALL STRAIGHT',
  LARGE_STRAIGHT: 'LARGE STRAIGHT',
};


const types = [
  {
    name: 'LARGE STRAIGHT',
    width: 700,
    length: 350,
    height: 200,
    payload: 4000,
  },
  {
    name: 'SMALL STRAIGHT',
    width: 500,
    length: 250,
    height: 170,
    payload: 2500,
  },
  {
    name: 'SPRINTER',
    width: 300,
    length: 250,
    height: 170,
    payload: 1700,
  },
];

module.exports.defineTruckType = (dimensions, payload) => {
  return types.filter((type) =>
    type.width > dimensions.width &&
    type.length > dimensions.length &&
    type.height > dimensions.height &&
    type.payload > payload).pop().name;
};
