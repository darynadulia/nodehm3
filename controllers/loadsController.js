/* eslint-disable camelcase */
const loadsDao = require('../models/dao/loadsDao');
const trucksDao = require('../models/dao/trucksDao');
const {IN_SERVICE} = require('../truckStatuses');
const {NEW, SHIPPED} = require('../loadStatuses');
const {defineNewState} = require('../loadStates');
const {ARRIVED_TO_DELIVERY} = require('../loadStates');

module.exports.showLoads = async (req, res) => {
  const {offset, limit} = req.query;
  const loads = await loadsDao.getLoads(req.user, limit, offset);
  res.json({loads});
};

module.exports.showLoad = async (req, res) => {
  const load = await loadsDao.getLoad(req.user, req.params.id);
  res.json({load});
};

module.exports.addLoad = async (req, res) => {
  const createdBy = req.user._id;
  const {
    name,
    payload,
    pickup_address: pickupAddress,
    delivery_address: deliveryAddress,
    dimensions,
  } = req.body;
  await loadsDao.createLoad(
      createdBy,
      name,
      payload,
      pickupAddress,
      deliveryAddress,
      dimensions,
  );
  res.json({message: 'Load created successfully'});
};

module.exports.updateLoad = async (req, res) => {
  const load = await loadsDao.getLoad(req.user, req.params.id);
  if (load.status !== NEW) {
    return res.status(403)
        .json({message: 'You can update only loads with NEW status'});
  }

  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = req.body;

  const update = {
    name: !name ? load.name : name,
    payload: !payload ? load.payload : payload,
    pickup_address: !pickup_address ? load.pickup_address : pickup_address,
    delivery_address: !delivery_address ?
      load.delivery_address :
      delivery_address,
    dimensions: {
      width: !dimensions || !dimensions.width ?
        load.dimensions.width :
        dimensions.width,
      length: !dimensions || !dimensions.length ?
        load.dimensions.length :
        dimensions.length,
      height: !dimensions || !dimensions.height ?
        load.dimensions.height :
        dimensions.height,
    },
  };

  await loadsDao.putLoad(req.params.id, update);
  res.json({message: 'Load details changed successfully'});
};

module.exports.removeLoad = async (req, res) => {
  const {status} = await loadsDao.getLoad(req.user, req.params.id);
  if (status !== NEW) {
    return res.status(403)
        .json({message: 'You can delete only loads with NEW status'});
  }
  await loadsDao.deleteLoad(req.user._id, req.params.id);
  res.json({message: 'Load deleted successfully'});
};

module.exports.postLoad = async (req, res) => {
  const load = await loadsDao.getLoad(req.user, req.params.id);
  if (load.status !== NEW) {
    return res.status(403).json({message: 'You can post only NEW loads'});
  }
  const truck = await trucksDao.findTruck(load.dimensions, load.payload);
  if (!truck) {
    await loadsDao.putLoad(load._id, {status: NEW});
    return res.json({message: 'No truck has been found', driver_found: false});
  } else {
    await loadsDao.putLoadAssignedTo(load._id, truck._id, truck.assigned_to);
    return res.json({
      message: `Load posted successfully`,
      driver_found: true,
    });
  }
};

module.exports.showShippingInfo = async (req, res) => {
  const [load] = await loadsDao.getShippingInfo(req.params.id);
  res.json({load});
};

module.exports.showActiveLoad = async (req, res) => {
  const load = await loadsDao.getActiveLoad(req.user._id);
  res.json({load});
};

module.exports.iterateState = async (req, res) => {
  const load = await loadsDao.getActiveLoad(req.user._id);
  if (!load) {
    return res.status(400).json({message: 'You don`t have the active load'});
  }
  const newState = defineNewState(load.state);
  if (newState === ARRIVED_TO_DELIVERY) {
    await loadsDao.putLoad(load._id, {state: newState, status: SHIPPED});
    await loadsDao.pushLog(load._id, 'Load shipped');
    await trucksDao.switchTruckStatus(load.assigned_to, IN_SERVICE);
    return res.json({message: 'Load shipped'});
  }
  await loadsDao.putLoad(load._id, {state: newState});
  await loadsDao.pushLog(load._id, `Load state changed to ${newState}`);
  res.json({message: `Load state changed to ${newState}`});
};
