const bcrypt = require('bcrypt');
const usersDao = require('../models/dao/usersDao');
// const {
//   deleteUser,
//   getUserByEmail,
//   changePassword,
// } = require('../models/dao/usersDao');


module.exports.showProfile = (req, res) => {
  res.json({user: {
    _id: req.user._id,
    email: req.user.email,
    created_date: req.user.created_date,
  }});
};

module.exports.deleteProfile = async (req, res) => {
  await usersDao.deleteUser(req.user._id);
  res.json({message: 'Profile deleted successfully'});
};

module.exports.password = async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const user = await usersDao.getUserByEmail(req.user.email);
  if (! (await bcrypt.compare(oldPassword, user.password))) {
    res.status(400).json({message: 'Wrong password'});
  }
  await usersDao.changePassword(user._id, newPassword);
  res.json({message: 'Success'});
};
