const trucksDao = require('../models/dao/trucksDao');

module.exports.addTruck = async (req, res) => {
  const {type} = req.body;
  await trucksDao.postTruck(type, req.user._id);
  res.json({message: 'Truck created successfully'});
};

module.exports.showTrucks = async (req, res) => {
  const trucks = await trucksDao.getTrucks(req.user._id);
  res.json({trucks});
};

module.exports.showTruck = async (req, res) => {
  const id = req.params.id;
  const truck = await trucksDao.getTruck(req.user._id, id);
  res.json({truck});
};

module.exports.changeTruckType = async (req, res) => {
  if (await trucksDao.checkAssignment(req.user._id, req.params.id)) {
    return res.status(403).json({message: 'Forbidden'});
  }
  await trucksDao.putTruckType(req.user._id, req.params.id, req.body.type);
  res.json({message: 'Truck details changed successfully'});
};

module.exports.removeTruck = async (req, res) => {
  if (await trucksDao.checkAssignment(req.user._id, req.params.id)) {
    return res.status(403).json({message: 'Forbidden'});
  }
  await trucksDao.deleteTruck(req.user._id, req.params.id);
  res.json({message: 'Truck deleted successfully'});
};

module.exports.asignTruck = async (req, res) => {
  await trucksDao.postTruckAsigned(req.user._id, req.params.id);
  res.json({message: 'Truck assigned successfully'});
};
