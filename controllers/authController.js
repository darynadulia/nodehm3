const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');
const usersDao = require('../models/dao/usersDao');

module.exports.registration = async (req, res) => {
  const {email, role, password} = req.body;
  await usersDao.postUser(email, role, password);
  res.json({message: 'Profile created successfully'});
};

module.exports.login = async (req, res) => {
  const {email, password} = req.body;

  const user = await usersDao.getUserByEmail(email);

  if (! (await bcrypt.compare(password, user.password))) {
    return res.status(400).json({message: 'Wrong password'});
  }
  const token = jwt.sign({
    _id: user._id,
    email: user.email,
    created_date: user.created_date,
    role: user.role,
  }, JWT_SECRET);
  res.json({jwt_token: token});
};


module.exports.forgotPassword = async (req, res) => {
  await usersDao.getUserByEmail(req.body.email);
  res.json({message: 'New password sent to your email address'});
};
