# NodeHM3

# Goal
Implement UBER like service for freight trucks, in REST style, using MongoDB as database. 

# Purpose
-  help people to deliver their stuff
- help drivers to find loads and earn some money

# Run application on local machine
- 'npm install'
- 'npm start'

use the swagger.yaml file to familiarize yourself with the api