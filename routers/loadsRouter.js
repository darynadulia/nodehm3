const express = require('express');
const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const userRoleMiddleware = require('./middlewares/userRoleMiddleware');
const loadsValidation = require('./middlewares/loadsValidation');
const loadsController = require('../controllers/loadsController');

const loadsRouter = new express.Router();

loadsRouter.use(asyncWrapper(authMiddleware));

loadsRouter.get('/', asyncWrapper(loadsController.showLoads));

loadsRouter.post('/',
    userRoleMiddleware.shipper,
    asyncWrapper(loadsValidation.load),
    asyncWrapper(loadsController.addLoad),
);

loadsRouter.get('/active',
    userRoleMiddleware.driver,
    asyncWrapper(loadsController.showActiveLoad));

loadsRouter.patch('/active/state',
    userRoleMiddleware.driver,
    asyncWrapper(loadsController.iterateState));

loadsRouter.get('/:id', asyncWrapper(loadsController.showLoad));

loadsRouter.put('/:id',
    userRoleMiddleware.shipper,
    asyncWrapper(loadsValidation.loadUpdate),
    asyncWrapper(loadsController.updateLoad));

loadsRouter.delete('/:id',
    userRoleMiddleware.shipper,
    asyncWrapper(loadsController.removeLoad));

loadsRouter.post('/:id/post',
    userRoleMiddleware.shipper,
    asyncWrapper(loadsController.postLoad));

loadsRouter.get('/:id/shipping_info',
    userRoleMiddleware.shipper,
    asyncWrapper(loadsController.showShippingInfo));

module.exports = loadsRouter;
