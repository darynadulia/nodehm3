module.exports = {
  asyncWrapper: function(callback) {
    return (req, res, next) => {
      callback(req, res, next)
          .catch(next);
    };
  },
};
