const joi = require('joi');

module.exports.load = async (req, res, next) => {
  const schema = joi.object({
    name: joi.string().required(),
    payload: joi.number().positive().required(),
    pickup_address: joi.string().required(),
    delivery_address: joi.string().required(),
    dimensions: joi.object({
      width: joi.number().positive().required(),
      height: joi.number().positive().required(),
      length: joi.number().positive().required(),
    }).required(),
  });
  await schema.validateAsync(req.body);
  next();
};

module.exports.loadUpdate = async (req, res, next) => {
  const schema = joi.object({
    name: joi.string(),
    payload: joi.number().positive(),
    pickup_address: joi.string(),
    delivery_address: joi.string(),
    dimensions: joi.object({
      width: joi.number().positive(),
      height: joi.number().positive(),
      length: joi.number().positive(),
    }),
  });
  await schema.validateAsync(req.body);
  next();
};
