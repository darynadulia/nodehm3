const {SHIPPER, DRIVER} = require('../../userRoles');

module.exports.shipper = (req, res, next) => {
  if (req.user.role != SHIPPER) {
    return res.status(403).json({message: 'Forbidden'});
  }
  next();
};

module.exports.driver = (req, res, next) => {
  if (req.user.role != DRIVER) {
    return res.status(403).json({message: 'Forbidden'});
  }
  next();
};
