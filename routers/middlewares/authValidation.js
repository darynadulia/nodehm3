const joi = require('joi');
const {SHIPPER, DRIVER} = require('../../userRoles');

module.exports.registration = async (req, res, next) => {
  const authSchema = joi.object({
    email: joi.string().email().required(),
    role: joi.string().valid(SHIPPER, DRIVER).required(),
    password: joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')).required(),
  });
  await authSchema.validateAsync(req.body);
  next();
};

module.exports.email = async (req, res, next) => {
  const schema = joi.object({
    email: joi.string().email().required(),
  });
  await schema.validateAsync(req.body);
  next();
};
