const joi = require('joi');
const {SPRINTER, SMALL_STRAIGHT, LARGE_STRAIGHT} = require('../../truckTypes');

module.exports.truck = async (req, res, next) => {
  const schema = joi.object({
    type: joi.string()
        .valid(SPRINTER, SMALL_STRAIGHT, LARGE_STRAIGHT)
        .required(),
  });
  await schema.validateAsync(req.body);
  next();
};
