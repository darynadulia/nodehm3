const express = require('express');
const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const userRoleMiddleware = require('./middlewares/userRoleMiddleware');
const truckValidation = require('./middlewares/trucksValidation');
const trucksController = require('../controllers/trucksController');

const truckRouter = new express.Router();

truckRouter.use('/',
    asyncWrapper(authMiddleware),
    userRoleMiddleware.driver);

truckRouter.get('/', asyncWrapper(trucksController.showTrucks));

truckRouter.post('/',
    asyncWrapper(truckValidation.truck),
    asyncWrapper(trucksController.addTruck));

truckRouter.get('/:id', asyncWrapper(trucksController.showTruck));

truckRouter.put('/:id',
    asyncWrapper(truckValidation.truck),
    asyncWrapper(trucksController.changeTruckType));

truckRouter.delete('/:id', asyncWrapper(trucksController.removeTruck));

truckRouter.post('/:id/assign', asyncWrapper(trucksController.asignTruck));

module.exports = truckRouter;
