const express = require('express');
const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const usersValidation = require('./middlewares/usersValidation');
const userRoleMiddleware = require('./middlewares/userRoleMiddleware');
const userController = require('../controllers/usersController');

const usersRouter = new express.Router();

usersRouter.use(asyncWrapper(authMiddleware));

usersRouter.get('/me', asyncWrapper(userController.showProfile));

usersRouter.delete('/me',
    userRoleMiddleware.shipper,
    asyncWrapper(userController.deleteProfile));

usersRouter.patch('/me/password',
    asyncWrapper(usersValidation.changePassword),
    asyncWrapper(userController.password));

module.exports = usersRouter;
