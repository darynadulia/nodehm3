/* eslint-disable require-jsdoc */
class UnauthorizedError extends Error {
  constructor(message = 'Unauthorized user!') {
    super(message);
    statusCode = 401;
  }
}

module.exports = UnauthorizedError;
