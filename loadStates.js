module.exports = {
  EN_ROUTE_TO_PICK_UP: 'En route to Pick Up',
  ARRIVED_TO_PICK_UP: 'Arrived to Pick Up',
  EN_ROUTE_TO_DELIVERY: 'En route to delivery',
  ARRIVED_TO_DELIVERY: 'Arrived to delivery',
};

module.exports.defineNewState = (oldState) => {
  const loadStates = [
    'En route to pick up',
    'Arrived to pick up',
    'En route to delivery',
    'Arrived to delivery',
  ];
  return loadStates[loadStates.indexOf(oldState) + 1];
};
