const {User} = require('../userModel');
const bcrypt = require('bcrypt');

module.exports.postUser = async (email, role, password) => {
  const user = new User({
    email,
    role,
    password: await bcrypt.hash(password, 10),
  });
  await user.save();
};

module.exports.getUserByEmail = async (email) => {
  const user = await User.findOne({email: email});
  if (!user) {
    throw new Error('No user with such email has been found');
  }
  return user;
};

module.exports.deleteUser = async (_id) => {
  await User.findByIdAndDelete(_id);
};

module.exports.changePassword = async (_id, newPassword) => {
  await User.findByIdAndUpdate(
      _id,
      {$set: {password: await bcrypt.hash(newPassword, 10)}},
  );
};
