const {Truck} = require('../truckModel');
const {ON_LOAD, IN_SERVICE} = require('../../truckStatuses');
const {defineTruckType} = require('../../truckTypes');

module.exports.postTruck = async (type, createdBy) => {
  const truck = new Truck({
    created_by: createdBy,
    type,
  });
  await truck.save();
};

module.exports.getTrucks = async (id) => {
  const trucks = await Truck.find({created_by: id});
  return trucks;
};

module.exports.getTruck = async (userId, truckId) => {
  const truck = await Truck.findOne({_id: truckId, created_by: userId});
  return truck;
};

module.exports.checkAssignment = async (userId, truckId) => {
  const {assigned_to: assignedTo} = await Truck.findOne(
      {_id: truckId, created_by: userId},
  );
  return assignedTo === userId ? true : false;
};

module.exports.putTruckType = async (userId, truckId, newType) => {
  await Truck.findOneAndUpdate({_id: truckId, created_by: userId},
      {$set: {type: newType}});
};

module.exports.deleteTruck = async (userId, truckId) => {
  await Truck.findOneAndDelete({_id: truckId, created_by: userId});
};

module.exports.postTruckAsigned = async (userId, truckId) => {
  await Truck.findOneAndUpdate({assigned_to: userId},
      {$set: {assigned_to: '', status: ON_LOAD}});
  await Truck.findOneAndUpdate({created_by: userId, _id: truckId},
      {$set: {assigned_to: userId, status: IN_SERVICE}});
};

module.exports.findTruck = async (dimensions, payload) => {
  const truckType = defineTruckType(dimensions, payload);
  const truck = await Truck.findOne({type: truckType, status: IN_SERVICE});
  return truck;
};

module.exports.putTruckStatus = async (truckId, newStatus) => {
  await Truck.findByIdAndUpdate(truckId, {$set: {status: newStatus}});
};

module.exports.switchTruckStatus = async (driverId, status) => {
  await Truck.findOneAndUpdate({assigned_to: driverId}, {$set: {status}});
};
