const mongoose = require('mongoose');
const {Load} = require('../loadModel');
const {DRIVER, SHIPPER} = require('../../userRoles');
const {putTruckStatus} = require('./trucksDao');
const {ON_LOAD} = require('../../truckStatuses');
const {ASSIGNED} = require('../../loadStatuses');
const {EN_ROUTE_TO_PICK_UP} = require('../../loadStates');

module.exports.getLoads = async (user, limit, offset) => {
  switch (user.role) {
    case DRIVER:
      return await Load.find(
          {assigned_to: user._id},
          [],
          {
            skip: +offset,
            limit: +limit,
          },
      );
    case SHIPPER:
      return await Load.find(
          {created_by: user._id},
          [],
          {
            skip: +offset,
            limit: +limit,
          },
      );
  };
};

module.exports.getLoad = async (user, loadId) => {
  switch (user.role) {
    case DRIVER: {
      return await Load.findOne({assigned_to: user._id, _id: loadId});
    }
    case SHIPPER: {
      return await Load.findOne({created_by: user._id, _id: loadId});
    }
  }
};

module.exports.createLoad = async (
    createdBy,
    name,
    payload,
    pickupAddress,
    deliveryAddress,
    dimensions) => {
  const load = new Load({
    created_by: createdBy,
    name,
    payload,
    pickup_address: pickupAddress,
    delivery_address: deliveryAddress,
    dimensions,
    logs: [{message: 'Load created', time: Date.now()}],
  });
  await load.save();
};

module.exports.putLoadAssignedTo = async (loadId, truckId, driverId) => {
  await putTruckStatus(truckId, ON_LOAD);
  const load = await Load.findById(loadId);
  load.assigned_to = driverId;
  load.status = ASSIGNED;
  load.state = EN_ROUTE_TO_PICK_UP;
  load.logs.push({
    message: `Load assigned to driver with id ${driverId}`,
    time: Date.now()});
  await load.save();
};

module.exports.putLoad = async (loadId, updates) => {
  await Load.findByIdAndUpdate(loadId, {$set: updates});
};

module.exports.deleteLoad = async (userId, loadId) => {
  await Load.findOneAndDelete({created_by: userId, _id: loadId});
};

module.exports.getShippingInfo = async (loadId) => {
  return await Load.aggregate([
    {$match: {_id: new mongoose.Types.ObjectId(loadId)}},
    {$lookup:
      {
        from: 'trucks',
        localField: 'assigned_to',
        foreignField: 'assigned_to',
        as: 'truck',
      }},
    {
      $project: {
        '_id': 1,
        'assigned_to': 1,
        'status': 1,
        'state': 1,
        'created_date': 1,
        'created_by': 1,
        'name': 1,
        'payload': 1,
        'pickup_address': 1,
        'delivery_address': 1,
        'dimensions': 1,
        'logs': 1,
        'truck': {'$arrayElemAt': ['$truck', 0]},
      },
    },
  ]);
};

module.exports.getActiveLoad = async (driverId) => {
  return await Load.findOne({assigned_to: driverId, status: ASSIGNED});
};

module.exports.pushLog = async (loadId, message) => {
  const load = await Load.findById(loadId);
  load.logs.push({message, time: Date.now()});
  await load.save();
};
