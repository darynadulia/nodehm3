const mongoose = require('mongoose');
const {IN_SERVICE} = require('../truckStatuses');

const truckSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    default: IN_SERVICE,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Truck = mongoose.model('Truck', truckSchema);
