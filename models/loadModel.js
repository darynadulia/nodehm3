const mongoose = require('mongoose');
const {NEW} = require('../loadStatuses');

const messageSchema = new mongoose.Schema({
  message: {type: String, required: true},
  time: {type: Date, default: Date.now()},
});

const loadSchema = new mongoose.Schema({
  created_by: {type: String, required: true},
  assigned_to: {type: String, default: null},
  status: {type: String, default: NEW},
  state: {type: String, default: null},
  name: {type: String, required: true},
  payload: {type: Number, required: true},
  pickup_address: {type: String, required: true},
  delivery_address: {type: String, required: true},
  dimensions: new mongoose.Schema({
    width: {type: Number, required: true},
    height: {type: Number, required: true},
    length: {type: Number, required: true},
  }),
  logs: [messageSchema],

  created_date: {type: Date, default: Date.now()},
});

module.exports.Load = mongoose.model('Load', loadSchema);
