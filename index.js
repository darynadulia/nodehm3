const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const UnauthorizedError = require('./customErrors');

const PORT = process.env.PORT || 8080;

const authRouter = require('./routers/authRouter');
const usersRouter = require('./routers/usersRouter');
const trucksRouter = require('./routers/trucksRouter');
const loadsRouter = require('./routers/loadsRouter');

const app = express();

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);

app.use((err, req, res, next) => {
  if (err instanceof UnauthorizedError) {
    res.status(err.statusCode).json({message: err.message});
  }
  res.status(400).json({message: err.message});
});

(async () => {
  await mongoose.connect('mongodb+srv://testuser:testuser@cluster0.llydq.mongodb.net/hm3?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(PORT);
})();
